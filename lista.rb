class Franquia

  def initialize
    @restaurantes = []
  end

  def adiciona(*restaurantes)
    for restaurante in restaurantes
      @restaurantes << restaurante
    end
  end

  def mostra
    for restaurante in @restaurantes
      puts restaurante.nome
    end
  end

end

class Restaurante
  attr_accessor :nome

  def initialize(nome)
    @nome = nome
  end

  def fechar_conta(dados)
    puts "Conta fechada no valor de #{dados[:valor]} e com 
    nota #{dados[:nota]}. Comentario: #{dados[:comentario]}"
  end
end

restaurante_1 = Restaurante.new("Fasano")
restaurante_2 = Restaurante.new("Fogo de chao")
restaurante_3 = Restaurante.new("Bar da chica")
restaurante_4 = Restaurante.new("bar do paim")
restaurante_5 = Restaurante.new("Bar cha da egua")

franquia = Franquia.new
franquia.adiciona(restaurante_1, restaurante_2, restaurante_3,
                  restaurante_4, restaurante_5)

franquia.mostra

restaurante_1.fechar_conta(valor: 50, nota: 9, comentario: "Massa")
