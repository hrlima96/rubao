class Restaurante
    attr_accessor :nota
    
    def initialize(nome)
        puts "criando um novo restaurante: #{nome}"
        @nome = nome
    end
    
  def qualifica(nota, msg="Obrigado!")
    puts "A nota do #{@nome} foi #{nota}. #{msg}"
  end
  
end

restaurante_um = Restaurante.new("Cantinho da felicidade")
restaurante_dois = Restaurante.new("Bar O rapadura")

restaurante_um.nota = 10
restaurante_dois.nota = 1

restaurante_um.qualifica(10)
restaurante_dois.qualifica("comida ruim")











